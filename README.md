# text-keyword-extract
> **A nodejs module to extract keywords from text.**

![logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/4904940/text-keyword-extract.png)

[![version](https://img.shields.io/npm/v/text-keyword-extract.svg)](https://www.npmjs.org/package/text-keyword-extract)
[![downloads](https://img.shields.io/npm/dt/text-keyword-extract.svg)](https://www.npmjs.org/package/text-keyword-extract)
[![node](https://img.shields.io/node/v/text-keyword-extract.svg)](https://nodejs.org/)
[![status](https://gitlab.com/autokent/text-keyword-extract/badges/master/pipeline.svg)](https://gitlab.com/autokent/text-keyword-extract/pipelines)

## Installation
`npm install text-keyword-extract`

## Usage

### Contains
```js
const tka = require('text-keyword-extract');

```


## Test
`mocha` or `npm test`

> check test folder and QUICKSTART.js for extra usage.